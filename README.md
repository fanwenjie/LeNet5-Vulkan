# LeNet5-Vulkan

#### 介绍
通过Vulkan的计算着色器编写LeNet5神经网络，并完成训练和推理的过程,根据YANN LECUN的论文《Gradient-based Learning Applied To Document Recognition》设计的LeNet-5神经网络，C语言写成，不依赖任何第三方库。 MNIST手写字符集训练识别率97%

#### DEMO
main.c文件为MNIST数据集的识别DEMO，直接编译即可运行，训练集60000张，测试集10000张。打开项目直接编译即可

#### 项目环境
1.  安装Visual Studio 2019
2.  安装Vulkan SDK

#### API 说明
##### 初始化设备上下文
ctx: 需要初始化的设备上下文
VkResult CreateDeviceContext(DeviceContext* ctx);

##### 销毁设备上下文
ctx: 需要销毁的设备上下文
void DestroyDeviceContext(DeviceContext* ctx);

##### 初始化训练缓存
ctx: 设备上下文
cache: 训练缓存
batchSize: 批量训练数
VkResult CreateTrainCache(DeviceContext* ctx, TrainCache* cache, const uint32_t batchSize);

##### 销毁训练缓存
ctx: 设备上下文
cache: 训练缓存
void DestroyTrainCache(DeviceContext* ctx, TrainCache* cache);

##### 从主存中加载模型到设备内存中
void LoadModel(DeviceContext* lenet, LeNet5* data);

##### 将设备内存中的模型加载到主存中
void SaveModel(DeviceContext* lenet, LeNet5* data);

##### 预测模型结果
ctx: 设备上下文
feature: 特征数据
uint32_t Predict(DeviceContext* ctx, Feature* feature);

##### 批量训练模型
ctx: 设备上下文
cache: 训练缓存
feature: 参与训练的特征数据
label: 训练的标签
void TrainBatch(DeviceContext* ctx, TrainCache* cache, Feature* feature, uint32_t* label);


#### 特别说明
内推字节跳动抖音/tiktok图形图像方面的人才，主要岗位有图形引擎开发、算法等，有意者请私信gitee.com/fanwenjie